PintOS Build Script for Docker
Robert Marmorstein (marmorsteinrm@longwood.edu)
2023-08-23

INTRODUCTION

This is a set of files which will allow you to use Docker to build the PintOS
pedagogical operating system.  It is intended to be modified and then used by
students in an undergraduate operating systems course.

By default, it fetches the official Pintos sources from Ben Pfaff's
pintos-os.org web site into a Debian linux container, patches them to compile,
installs compatible versions of the bochs and qemu emulators and builds the
pintos utilities.

It also installs .bashrc and .bash_logout files that automatically fetch the
latest code from a git repository and push any changes back to that repository.

USAGE

To use this code, first create a fork of the pintos source code on a git server
(such as gitlab.com or github.com) and set up a public-key/private-key pair for
authentication.  Save the id_rsa.pub and id_rsa files to the same folder the
Dockerfile is in.

Then edit Dockerfile to use your real name and email address.  Also modify the
git clone command to pull from your new fork of the pintos source code.  Then
uncomment the lines that copy id_rsa and id_rsa.pub into /home/pintos/.ssh/.
You may find it convenient to also create and copy in a "config" file into the
.ssh folder.  This allows you, for instance, to set a custom username, port
number, or host alias for connecting to the git server.  I also recommend
adding a known_hosts file that includes host credentials for the git server so
that you don't have to type "yes" when pulling from the server.

Once you have finished editing the Dockerfile, use:

docker build -t pintos .

To build it and:

docker run --rm -it --name pintos pintos

to run it.
