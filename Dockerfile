FROM debian:bookworm as devbase
RUN apt-get -y update 
RUN apt-get -y upgrade
RUN apt-get -y install build-essential libx11-dev libwxgtk3.2-dev file xorg-dev wget git qemu-system qemu-system-x86 qemu-utils perl gdb vim libncurses-dev libsdl1.2-dev 
RUN apt-get -y install libc6-dev

RUN useradd -m pintos

#RUN mkdir /home/pintos/.ssh
#COPY config /home/pintos/.ssh/
#COPY known_hosts /home/pintos/.ssh/
#COPY id_rsa /home/pintos/.ssh/
#RUN chown -R pintos:pintos /home/pintos/.ssh && chmod 600 /home/pintos/.ssh/config /home/pintos/.ssh/known_hosts /home/pintos/.ssh/id_rsa

USER pintos:pintos
WORKDIR /home/pintos

RUN git config --global user.email "pintos@example.com" 	#Change this to your own email address
RUN git config --global user.name "Pintos Developer" 		#Change this to your own name
RUN git config --global pull.rebase false \
	&& git clone git://pintos-os.org/pintos-anon pintos	#Change this to point to your own fork

COPY bashrc /home/pintos/.bashrc
COPY bash_logout /home/pintos/.bash_logout
COPY vimrc /home/pintos/.vimrc

WORKDIR /home/pintos/pintos/src/utils

COPY squish-pty.patch squish-pty.patch
RUN patch -p0 < squish-pty.patch
COPY squish-unix.patch squish-unix.patch
RUN patch -p0 < squish-unix.patch
RUN make

ENV PATH="${PATH}:/home/pintos/pintos/src/utils"

USER root:root
WORKDIR /home/pintos/pintos/src/misc
RUN env SRCDIR=/tmp PINTOSDIR=/home/pintos/pintos DSTDIR=/usr/local sh ./bochs-2.6.11-build.sh /usr/local

USER pintos:pintos

EXPOSE 1234

WORKDIR /home/pintos/pintos/src
CMD /bin/bash -l
